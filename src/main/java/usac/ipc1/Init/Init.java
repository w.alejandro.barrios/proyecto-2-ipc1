package usac.ipc1.Init;

import usac.ipc1.Nodos.*;
import usac.ipc1.Principal;
import usac.ipc1.beans.Empleado;
import usac.ipc1.views.LoginView;

import javax.swing.*;

public class Init {

    public Init(){

        // ---- ORDEN
        Principal.listaOrden = new ListaOrden();
        Principal.contadorOrdenes = 0;
        // ---- EMPLEADOS
        Principal.contadorEmpleados = 0;
        Principal.listaEmpleados = new ListaEmpleados();
        // ---- CLIENTES
        Principal.contadorClientes = 0;
        Principal.listaClientes = new ListaClientes();
        // ---- REPUESTOS
        Principal.contadorRepuestos = 0;
        Principal.pilaRepuestos = new PilaRepuestos();
        // ---- SERVICIOS
        Principal.contadorServicios = 0;
        Principal.listaServicios = new ListaServicios();
        // ---- AUTOMOVILES
        Principal.contadorAutomoviles = 0;
        Principal.listaAutomoviles = new ListaAutomoviles();
        // ---- COLA DE ESPERA
        Principal.contadorColaEspera = 0;
        Principal.colaEspera = new ColaEspera();
        // ---- COLA DE AUTOS LISTOS
        Principal.contadorColaAutosListos = 0;
        Principal.colaAutosListos = new ColaAutosListos();
        // ---- INICIALIZACIONES
        Principal.listaEmpleados.insertarFinal(new Empleado(Principal.contadorEmpleados, "Admin", 2, "admin", "admin"));
        Principal.contadorEmpleados++;

        SwingUtilities.invokeLater(() -> new LoginView());
    }

}
