package usac.ipc1.Init;

import usac.ipc1.Principal;
import usac.ipc1.beans.*;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class ReadFile {

    public ReadFile(File file, Frame frame, Table.TipoTabla tipoTabla) {
        read(file, frame, tipoTabla);
    }

    public void read(File file, Frame frame, Table.TipoTabla tipoTabla) {
        String lineFile = null;

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((lineFile = bufferedReader.readLine()) != null) {
                String[] lineReaded = lineFile.split("-");
                switch (tipoTabla) {
                    case EMPLEADOS:
                        int rol = 0;
                        switch (lineReaded[1]) {
                            case "mecanico":
                                rol = 1;
                                break;

                            case "receptorPagador":
                                rol = 3;
                                break;

                            case "Administrador":
                                rol = 2;
                                break;
                        }
                        Principal.listaEmpleados.insertarFinal(new Empleado(Principal.contadorEmpleados, lineReaded[0], rol, lineReaded[2], lineReaded[3]));
                        Principal.contadorEmpleados++;
                        break;

                    case REPUESTOS:
                        Principal.pilaRepuestos.apilar(
                                new Repuesto(Principal.contadorRepuestos,
                                        lineReaded[0],
                                        lineReaded[1],
                                        lineReaded[2],
                                        Integer.parseInt(lineReaded[3]),
                                        Double.parseDouble(lineReaded[4])
                                )
                        );
                        Principal.contadorRepuestos++;
                        break;

                    case CLIENTES:
                    case CLIENTESAUTOS:
                        int tipoCliente = 0;
                        if (lineReaded[4].equals("oro"))
                            tipoCliente = 2;
                        else if (lineReaded[4].equals("normal"))
                            tipoCliente = 1;
                        Cliente cliente = new Cliente(lineReaded[0], lineReaded[1], lineReaded[2], lineReaded[3], tipoCliente, 0);
                        Principal.listaClientes.insertarFinal(cliente);
                        Principal.contadorClientes++;
                        String[] quantityAutos = lineReaded[5].split(";");
                        System.out.println(quantityAutos.length);
                        for (int i = 0; i < quantityAutos.length; i++) {
                            String[] caractAuto = quantityAutos[i].split(",");
                            Principal.listaAutomoviles.insertarFinal(
                                    new Automovil(caractAuto[0],
                                            caractAuto[1],
                                            caractAuto[2],
                                            caractAuto[3],
                                            cliente)
                            );
                            Principal.contadorAutomoviles++;
                        }
                        break;

                    case SERVICIOS:
                        Double precioRepuestos = 0.0;
                        String[] repuesto = lineReaded[3].split(";");
                        for (int i = 0; i < repuesto.length; i++) {
                            Repuesto getRepuesto = Principal.pilaRepuestos.buscarRepuesto(Integer.parseInt(repuesto[i]));
                            precioRepuestos += getRepuesto.getPrecio();
                            getRepuesto.setExistencias(getRepuesto.getExistencias() - 1);
                            getRepuesto.setUso(getRepuesto.getUso() + 1);
                        }

                        Principal.listaServicios.insertarFinal(
                                new Servicio(Principal.contadorServicios,
                                        lineReaded[0],
                                        lineReaded[1],
                                        lineReaded[2],
                                        Double.parseDouble(lineReaded[4]),
                                        lineReaded[3],
                                        precioRepuestos + Double.parseDouble(lineReaded[4]))
                        );
                        Principal.contadorServicios++;
                        break;
                }
            }

            bufferedReader.close();
            JOptionPane.showMessageDialog(frame,
                    file.getName() + " cargado exitosamente",
                    "Configuracion",
                    JOptionPane.WARNING_MESSAGE);

        } catch (FileNotFoundException e) {
            System.out.println("No se puede abrir el archivo " + file.getName());
            e.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error leyendo archivo " + file.getName());
            ioe.printStackTrace();
        }
    }
}
