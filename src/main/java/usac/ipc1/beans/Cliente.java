package usac.ipc1.beans;

import java.math.BigInteger;

public class Cliente {

    private String dpi;
    private String nombre;
    private String username;
    private String password;
    private int tipoCliente; // 1 = Normal; 2 = Oro;
    private int quantityServicios;

    public Cliente() {
    }

    public Cliente(String dpi, String nombre, String username, String password, int tipoCliente, int quantityServicios) {
        this.dpi = dpi;
        this.nombre = nombre;
        this.username = username;
        this.password = password;
        this.tipoCliente = tipoCliente;
        this.quantityServicios = quantityServicios;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(int tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public int getQuantityServicios() {
        return quantityServicios;
    }

    public void setQuantityServicios(int quantityServicios) {
        this.quantityServicios = quantityServicios;
    }
}
