package usac.ipc1.beans;

public class Servicio {

    private int idServicio;
    private String nombre;
    private String marca;
    private String modelo;
    private double manoDeObra;
    private String pilaRepuestos;
    private double precioTotal;

    public Servicio() {
    }

    public Servicio(int idServicio, String nombre, String marca, String modelo, double manoDeObra, String pilaRepuestos, double precioTotal) {
        this.idServicio = idServicio;
        this.nombre = nombre;
        this.marca = marca;
        this.modelo = modelo;
        this.manoDeObra = manoDeObra;
        this.pilaRepuestos = pilaRepuestos;
        this.precioTotal = precioTotal;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getManoDeObra() {
        return manoDeObra;
    }

    public void setManoDeObra(double manoDeObra) {
        this.manoDeObra = manoDeObra;
    }

    public String getPilaRepuestos() {
        return pilaRepuestos;
    }

    public void setPilaRepuestos(String pilaRepuestos) {
        this.pilaRepuestos = pilaRepuestos;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }
}
