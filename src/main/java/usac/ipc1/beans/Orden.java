package usac.ipc1.beans;

import java.util.Date;

public class Orden {

    private int idOrden;
    private Automovil automovil;
    private Cliente cliente;
    private Servicio servicio;
    private Date fecha;
    private Empleado empleado;
    private int estado; // 1 = Cola de espera; 2 = En servicio; 3 = Listo;

    public Orden() {
    }

    public Orden(int idOrden, Automovil automovil, Cliente cliente, Servicio servicio, Date fecha, Empleado empleado, int estado) {
        this.idOrden = idOrden;
        this.automovil = automovil;
        this.cliente = cliente;
        this.servicio = servicio;
        this.fecha = fecha;
        this.empleado = empleado;
        this.estado = estado;
    }

    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public Automovil getAutomovil() {
        return automovil;
    }

    public void setAutomovil(Automovil automovil) {
        this.automovil = automovil;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
