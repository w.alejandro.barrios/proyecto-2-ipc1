package usac.ipc1.beans;

public class Empleado {

    private int idEmpleado;
    private String name;
    private int rol; // 1 = Mecánico; 2 = Administrador; 3 = Receptor/Pagador
    private String username;
    private String password;
    private boolean ocupado = false;
    private Orden orden = null;

    public Empleado() {
    }

    public Empleado(int idEmpleado, String name, int rol, String username, String password) {
        this.idEmpleado = idEmpleado;
        this.name = name;
        this.rol = rol;
        this.username = username;
        this.password = password;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }
}
