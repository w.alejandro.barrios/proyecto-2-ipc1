package usac.ipc1.models;

import usac.ipc1.Nodos.ListaEmpleados;
import usac.ipc1.beans.Empleado;

import javax.swing.table.AbstractTableModel;

public class TablaEmpleados extends AbstractTableModel {

    private ListaEmpleados listaEmpleados;
    private String[] columnName = new String[]{"Id.", "Nombre", "Rol", "Username", "Contraseña"};

    public TablaEmpleados(ListaEmpleados listaEmpleados){
        this.listaEmpleados = listaEmpleados;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.listaEmpleados.lengthLista();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Empleado empleado = this.listaEmpleados.buscarPosicion(rowIndex);
        if (empleado != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(empleado.getIdEmpleado());
                    break;

                case 1:
                    value = empleado.getName();
                    break;

                case 2:
                    value = Integer.toString(empleado.getRol());
                    break;

                case 3:
                    value = empleado.getUsername();
                    break;

                case 4:
                    value = empleado.getPassword();
                    break;
            }
        }
        return value;
    }
}
