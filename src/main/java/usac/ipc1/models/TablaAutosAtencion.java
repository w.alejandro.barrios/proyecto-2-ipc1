package usac.ipc1.models;

import usac.ipc1.Nodos.ListaOrden;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Orden;

import javax.swing.table.AbstractTableModel;

public class TablaAutosAtencion extends AbstractTableModel {

    private ListaOrden listaOrden;
    private Cliente cliente;
    private String[] columnName = new String[]{"Orden.", "Automovil", "Cliente", "Mecanico", "Estado"};

    public TablaAutosAtencion(ListaOrden listaOrden, Cliente cliente){
        this.listaOrden = listaOrden;
        this.cliente = cliente;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.listaOrden.lengthLista();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Orden orden = this.listaOrden.buscarPosicion(rowIndex);
        if (cliente != null){
            if (orden != null){
                if (orden.getCliente() == cliente){
                    switch (columnIndex){
                        case 0:
                            value = "" + orden.getIdOrden();
                            break;

                        case 1:
                            value = orden.getAutomovil().getMarca() + " " + orden.getAutomovil().getModelo();
                            break;

                        case 2:
                            value = orden.getCliente().getNombre();
                            break;

                        case 3:
                            if (orden.getEmpleado() != null)
                                value = orden.getEmpleado().getName();
                            else
                                value = "Sin asignar";
                            break;

                        case 4:
                            switch (orden.getEstado()){
                                case 1:
                                    value = "Cola de espera";
                                    break;

                                case 2:
                                    value = "En servicio";
                                    break;

                                case 3:
                                    value = "Listo";
                                    break;
                            }
                            break;
                    }
                }
            }
        }else{
            if (orden != null){
                if (orden.getEstado() == 2){
                    switch (columnIndex){
                        case 0:
                            value = "" + orden.getIdOrden();
                            break;

                        case 1:
                            value = orden.getAutomovil().getMarca() + " " + orden.getAutomovil().getModelo();
                            break;

                        case 2:
                            value = orden.getCliente().getNombre();
                            break;

                        case 3:
                            value = orden.getEmpleado().getName();
                            break;

                        case 4:
                            switch (orden.getEstado()){
                                case 1:
                                    value = "Cola de espera";
                                    break;

                                case 2:
                                    value = "En servicio";
                                    break;

                                case 3:
                                    value = "Listo";
                                    break;
                            }
                            break;
                    }
                }
            }
        }

        return value;
    }
}
