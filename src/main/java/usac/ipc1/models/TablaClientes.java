package usac.ipc1.models;

import usac.ipc1.Nodos.ListaClientes;
import usac.ipc1.beans.Cliente;

import javax.swing.table.AbstractTableModel;

public class TablaClientes extends AbstractTableModel {

    private ListaClientes listaClientes;
    private String[] columnName = new String[]{ "DPI", "Nombre", "Username", "Password", "Tipo" };

    public TablaClientes(ListaClientes listaClientes){
        this.listaClientes = listaClientes;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.listaClientes.lengthLista();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Cliente cliente = this.listaClientes.buscarPosicion(rowIndex);
        if (cliente != null){
            switch (columnIndex){
                case 0:
                    value = cliente.getDpi();
                    break;

                case 1:
                    value = cliente.getNombre();
                    break;

                case 2:
                    value = cliente.getUsername();
                    break;

                case 3:
                    value = cliente.getPassword();
                    break;

                case 4:
                    value = Integer.toString(cliente.getTipoCliente());
                    break;
            }
        }
        return value;
    }
}
