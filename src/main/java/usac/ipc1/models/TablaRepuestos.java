package usac.ipc1.models;

import usac.ipc1.Nodos.PilaRepuestos;
import usac.ipc1.beans.Repuesto;

import javax.swing.table.AbstractTableModel;

public class TablaRepuestos extends AbstractTableModel {

    private PilaRepuestos pilaRepuestos;
    private String[] columnName = new String[]{ "Id.", "Nombre", "Marca", "Modelo", "Existencias", "Precio" };

    public TablaRepuestos(PilaRepuestos pilaRepuestos){
        this.pilaRepuestos = pilaRepuestos;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.pilaRepuestos.lengthPila();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Repuesto repuesto = this.pilaRepuestos.recorrer(rowIndex);
        if (repuesto != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(repuesto.getIdRepuesto());
                    break;

                case 1:
                    value = repuesto.getName();
                    break;

                case 2:
                    value = repuesto.getMarca();
                    break;

                case 3:
                    value = repuesto.getModelo();
                    break;

                case 4:
                    value = Integer.toString(repuesto.getExistencias());
                    break;

                case 5:
                    value = Double.toString(repuesto.getPrecio());
                    break;
            }
        }
        return value;
    }
}
