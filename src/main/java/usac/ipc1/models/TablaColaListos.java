package usac.ipc1.models;

import usac.ipc1.Nodos.ColaAutosListos;
import usac.ipc1.beans.Orden;

import javax.swing.table.AbstractTableModel;

public class TablaColaListos extends AbstractTableModel {

    private ColaAutosListos colaAutosListos;
    private String[] columnName = new String[]{"Orden.", "Automovil", "Cliente", "Servicio", "Total"};

    public TablaColaListos(ColaAutosListos colaAutosListos){
        this.colaAutosListos = colaAutosListos;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.colaAutosListos.lengthCola();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Orden orden = this.colaAutosListos.recorrer(rowIndex);
        if (orden != null){
            switch (columnIndex){
                case 0:
                    value = "" + orden.getIdOrden();
                    break;

                case 1:
                    value = orden.getAutomovil().getMarca() + " " + orden.getAutomovil().getModelo();
                    break;

                case 2:
                    value = orden.getCliente().getNombre();
                    break;

                case 3:
                    value = orden.getServicio().getNombre();
                    break;

                case 4:
                    value = "" + orden.getServicio().getPrecioTotal();
                    break;
            }
        }
        return value;
    }
}
