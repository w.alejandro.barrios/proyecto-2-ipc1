package usac.ipc1.models;

import usac.ipc1.Nodos.ListaServicios;
import usac.ipc1.beans.Servicio;

import javax.swing.table.AbstractTableModel;

public class TablaServicios extends AbstractTableModel {

    private ListaServicios listaServicios;
    private String[] columnName = new String[]{ "Id.", "Nombre", "Marca", "Modelo", "Mano de obra", "Precio Total" };

    public TablaServicios(ListaServicios listaServicios){
        this.listaServicios = listaServicios;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.listaServicios.lengthLista();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Servicio servicio = this.listaServicios.buscarPosicion(rowIndex);
        if (servicio != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(servicio.getIdServicio());
                    break;

                case 1:
                    value = servicio.getNombre();
                    break;

                case 2:
                    value = servicio.getMarca();
                    break;

                case 3:
                    value = servicio.getModelo();
                    break;

                case 4:
                    value = Double.toString(servicio.getManoDeObra());
                    break;

                case 5:
                    value = Double.toString(servicio.getPrecioTotal());
                    break;
            }
        }
        return value;
    }
}
