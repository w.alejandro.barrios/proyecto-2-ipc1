package usac.ipc1.models;

import usac.ipc1.Nodos.ColaEspera;
import usac.ipc1.beans.Orden;

import javax.swing.table.AbstractTableModel;

public class TablaColaEspera extends AbstractTableModel {

    private ColaEspera colaEspera;
    private String[] columnName = new String[]{"Orden.", "Automovil", "Cliente"};

    public TablaColaEspera(ColaEspera colaEspera){
        this.colaEspera = colaEspera;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.colaEspera.lengthCola();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Orden orden = this.colaEspera.recorrer(rowIndex);
        if (orden != null){
            switch (columnIndex){
                case 0:
                    value = "" + orden.getIdOrden();
                    break;

                case 1:
                    value = orden.getAutomovil().getMarca() + " " + orden.getAutomovil().getModelo();
                    break;

                case 2:
                    value = orden.getCliente().getNombre();
                    break;
            }
        }
        return value;
    }
}
