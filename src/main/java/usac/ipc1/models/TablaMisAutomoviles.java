package usac.ipc1.models;

import usac.ipc1.Nodos.ListaAutomoviles;
import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;

import javax.swing.table.AbstractTableModel;

public class TablaMisAutomoviles extends AbstractTableModel {

    private ListaAutomoviles listaAutomoviles;
    private String[] columnName = new String[]{ "Placa", "Marca", "Modelo", "Imagen" };

    public TablaMisAutomoviles(ListaAutomoviles listaAutomoviles){
        this.listaAutomoviles = listaAutomoviles;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.listaAutomoviles.lengthLista();
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Automovil automovil = this.listaAutomoviles.buscarPosicion(rowIndex);
        if (automovil != null){
            if (automovil.getCliente() == Principal.loguedInC){
                switch (columnIndex){
                    case 0:
                        value = automovil.getPlaca();
                        break;

                    case 1:
                        value = automovil.getMarca();
                        break;

                    case 2:
                        value = automovil.getModelo();
                        break;
                    case 3:
                        value = automovil.getRutaImagen();
                        break;
                }
            }

        }
        return value;
    }

}
