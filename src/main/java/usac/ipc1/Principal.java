package usac.ipc1;

import usac.ipc1.Nodos.*;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Empleado;

public class Principal {

    // ---- ORDEN
    public static ListaOrden listaOrden;
    public static int contadorOrdenes;
    // ---- EMPLEADOS
    public static ListaEmpleados listaEmpleados;
    public static int contadorEmpleados;
    // ---- CLIENTES
    public static ListaClientes listaClientes;
    public static int contadorClientes;
    // ---- REPUESTOS
    public static PilaRepuestos pilaRepuestos;
    public static int contadorRepuestos;
    // ---- SERVICIOS
    public static ListaServicios listaServicios;
    public static int contadorServicios;
    // ---- AUTOMOVILES
    public static ListaAutomoviles listaAutomoviles;
    public static int contadorAutomoviles;
    // ---- COLA DE ESPERA
    public static ColaEspera colaEspera;
    public static int contadorColaEspera;
    // ---- COLA DE AUTOS LISTOS
    public static ColaAutosListos colaAutosListos;
    public static int contadorColaAutosListos;
    // ---- LOGUEO
    public static Empleado loguedInE;
    public static Cliente loguedInC;

}
