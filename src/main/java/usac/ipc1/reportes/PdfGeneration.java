package usac.ipc1.reportes;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import usac.ipc1.Nodos.ListaOrden;
import usac.ipc1.Nodos.ListaServicios;
import usac.ipc1.Nodos.PilaRepuestos;
import usac.ipc1.Principal;
import usac.ipc1.beans.Orden;
import usac.ipc1.beans.Repuesto;
import usac.ipc1.beans.Servicio;
import usac.ipc1.ordenamientos.RepuestosReorder;
import usac.ipc1.ordenamientos.ServiciosMasUsados;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdfGeneration {

    public static String DEST = "results/chapter01/Report.pdf";

    public PdfGeneration(int selection) {
        File file = new File(DEST);
        file.getParentFile().mkdirs();

        try {
            createPdf(DEST, selection);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createPdf(String destination, int selection) throws IOException {
        // Initialize PDF
        FileOutputStream fileOutputStream = new FileOutputStream(destination);
        PdfWriter writer = new PdfWriter(fileOutputStream);

        // Initialize pdf document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf);

        // Content
        float[] pointColumnWidths = {40F, 100F, 75F, 75F, 75F, 75F};
        Table table = new com.itextpdf.layout.element.Table(pointColumnWidths);
        Cell cel = new Cell();
        Cell cel2 = new Cell();
        Cell cel3 = new Cell();
        Cell cel4 = new Cell();
        Cell cel5 = new Cell();
        Cell cel6 = new Cell();

        ListaOrden listaOrden = Principal.listaOrden;
        ListaServicios listaServicios = Principal.listaServicios;
        PilaRepuestos pilaRepuestos = Principal.pilaRepuestos;

        switch (selection) {
            case 1: // TOP 10 REPUESTOS MAS USADOS
                document.add(new Paragraph("TOP 10 REPUESTOS MAS USADOS"));
                document.add(new Paragraph(""));

                cel.add("No.");
                cel2.add("Nombre");
                cel3.add("Marca");
                cel4.add("Modelo");
                cel5.add("Existencias");
                cel6.add("Precio");

                RepuestosReorder repuestosMasUsados = new RepuestosReorder();
                for (int i = 0; i < pilaRepuestos.lengthPila(); i++){
                    Repuesto repuesto = pilaRepuestos.buscarPosicion(i);

                    if (repuestosMasUsados.lengthLista() > 0){
                        if (repuesto.getUso() > pilaRepuestos.buscarPosicion(i-1).getUso()){
                            repuestosMasUsados.insertarPrincipio(repuesto);
                        }else{
                            repuestosMasUsados.insertarFinal(repuesto);
                        }
                    }else{
                        repuestosMasUsados.insertarFinal(repuesto);
                    }

                }

                int contadorRepuestos = 1;
                System.out.println(repuestosMasUsados.lengthLista());
                for (int i = 0; i < repuestosMasUsados.lengthLista(); i++) {
                    Repuesto imprimir = repuestosMasUsados.buscarPosicion(i);
                    System.out.println("ID SERVICIO: " + imprimir.getIdRepuesto());
                    cel.add("" + contadorRepuestos );
                    cel2.add(imprimir.getName());
                    cel3.add(imprimir.getMarca());
                    cel4.add(imprimir.getModelo());
                    cel5.add("" + imprimir.getExistencias());
                    cel6.add("Q" + imprimir.getPrecio());
                    if (contadorRepuestos == 10){
                        break;
                    }
                    contadorRepuestos++;
                }

                table.addCell(cel);
                table.addCell(cel2);
                table.addCell(cel3);
                table.addCell(cel4);
                table.addCell(cel5);
                table.addCell(cel6);

                break;

            case 2: // TOP 10 SERVICIOS MAS USADOS
                document.add(new Paragraph("TOP 10 SERVICIOS MAS USADOS"));
                document.add(new Paragraph(""));

                cel.add("No.");
                cel2.add("Nombre");
                cel3.add("Marca");
                cel4.add("Modelo");
                cel5.add("Mano de obra");
                cel6.add("Total");

                int servicioAnterior = 0;
                ServiciosMasUsados serviciosMasUsados = new ServiciosMasUsados();

                for (int i = 0; i < listaServicios.lengthLista(); i++) {

                    Servicio servicio = listaServicios.buscarPosicion(i);
                    int servicioUsado = 0;
                    for (int o = 0; o < listaOrden.lengthLista(); o++) {
                        Orden orden = listaOrden.buscarPosicion(o);
                        if (servicio == orden.getServicio()) {
                            servicioUsado++;
                        }

                    }

                    if (servicioUsado > 0) {
                        if (serviciosMasUsados.lengthLista() > 0) {
                            if (servicioAnterior > servicioUsado) {
                                serviciosMasUsados.insertarFinal(servicio);
                            } else {
                                serviciosMasUsados.insertarPrincipio(servicio);
                            }
                        } else {
                            serviciosMasUsados.insertarFinal(servicio);
                        }
                    }
                    servicioAnterior = servicioUsado;
                }

                int contador = 1;
                System.out.println(serviciosMasUsados.lengthLista());
                for (int i = 0; i < serviciosMasUsados.lengthLista(); i++) {
                    Servicio imprimir = serviciosMasUsados.buscarPosicion(i);
                    System.out.println("ID SERVICIO: " + imprimir.getIdServicio());
                    cel.add("" + contador );
                    cel2.add(imprimir.getNombre());
                    cel3.add(imprimir.getMarca());
                    cel4.add(imprimir.getModelo());
                    cel5.add("Q" + imprimir.getManoDeObra());
                    cel6.add("Q" + imprimir.getPrecioTotal());
                    if (contador == 10){
                        break;
                    }
                    contador++;
                }

                table.addCell(cel);
                table.addCell(cel2);
                table.addCell(cel3);
                table.addCell(cel4);
                table.addCell(cel5);
                table.addCell(cel6);

                break;
        }

        document.add(table);

        // Close document
        document.close();

        File myFile = new File(destination);
        Desktop.getDesktop().open(myFile);
    }

}
