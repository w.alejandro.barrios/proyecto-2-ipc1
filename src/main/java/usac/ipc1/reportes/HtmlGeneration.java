package usac.ipc1.reportes;

import usac.ipc1.Nodos.ListaAutomoviles;
import usac.ipc1.Nodos.ListaClientes;
import usac.ipc1.Nodos.PilaRepuestos;
import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Repuesto;
import usac.ipc1.ordenamientos.AutosMasRepetidos;
import usac.ipc1.ordenamientos.RepuestosReorder;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HtmlGeneration {

    public static String DEST = "results/reportes/reportHtml.html";

    public HtmlGeneration(int selection){
        File file = new File(DEST);
        file.getParentFile().mkdirs();

        try {
            createHtml(DEST, selection);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void createHtml(String destination, int selection) throws IOException{
        BufferedWriter writer = null;
        File file = new File(destination);

        writer = new BufferedWriter(new FileWriter(file));
        // --- START HEADER
        writer.write("<!DOCTYPE html>");
        writer.write("<html>");
        writer.write("<head>");
        writer.write("<meta charset='UTF-8'>");
        writer.write("<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>");
        writer.write("<title>Reporte en HTML</title>");
        writer.write("</head>");
        // --- END HEADER

        // --- START BODY
        writer.write("<body>");

        // --- CONTENT
        switch (selection){
            case 1: // CLIENTES
                ListaClientes listaClientes = Principal.listaClientes;

                writer.write("<h1>CLIENTES</h1>");
                writer.write("<hr />");

                writer.write("<h3>Clientes normales</h3>");

                writer.write("<table class='table'>");

                writer.write("<tr>");
                writer.write("<th>DPI</th>");
                writer.write("<th>Nombre</th>");
                writer.write("<th>Username</th>");
                writer.write("<th>Password</th>");
                writer.write("</tr>");

                for (int i = 0; i < listaClientes.lengthLista(); i++){
                    Cliente cliente = listaClientes.buscarPosicion(i);
                    if (cliente.getTipoCliente() == 1){
                        writer.write("<tr>");
                        writer.write("<th>" + cliente.getDpi() + "</th>");
                        writer.write("<th>" + cliente.getNombre() + "</th>");
                        writer.write("<th>" + cliente.getUsername() + "</th>");
                        writer.write("<th>" + cliente.getPassword() + "</th>");
                        writer.write("</tr>");
                    }
                }

                writer.write("</table>");

                writer.write("<h3>Clientes Oro</h3>");

                writer.write("<table class='table'>");

                writer.write("<tr>");
                writer.write("<th>DPI</th>");
                writer.write("<th>Nombre</th>");
                writer.write("<th>Username</th>");
                writer.write("<th>Password</th>");
                writer.write("</tr>");

                for (int i = 0; i < listaClientes.lengthLista(); i++){
                    Cliente cliente = listaClientes.buscarPosicion(i);
                    if (cliente.getTipoCliente() == 2){
                        writer.write("<tr>");
                        writer.write("<th>" + cliente.getDpi() + "</th>");
                        writer.write("<th>" + cliente.getNombre() + "</th>");
                        writer.write("<th>" + cliente.getUsername() + "</th>");
                        writer.write("<th>" + cliente.getPassword() + "</th>");
                        writer.write("</tr>");
                    }
                }

                writer.write("</table>");
                break;

            case 2: // TOP 10 REPUESTOS MAS CAROS
                PilaRepuestos pilaRepuestos = Principal.pilaRepuestos;
                RepuestosReorder repuestosMasCaros = new RepuestosReorder();

                writer.write("<h1>TOP 10 REPUESTOS MAS CAROS</h1>");
                writer.write("<hr />");

                writer.write("<table class='table'>");

                writer.write("<tr>");
                writer.write("<th>Nombre</th>");
                writer.write("<th>Marca</th>");
                writer.write("<th>Modelo</th>");
                writer.write("<th>Existencias</th>");
                writer.write("<th>Precio</th>");
                writer.write("</tr>");

                for (int i = 0; i < pilaRepuestos.lengthPila(); i++){
                    Repuesto repuesto = pilaRepuestos.buscarPosicion(i);

                    if (repuesto != null){
                        if (repuestosMasCaros.lengthLista() > 0){
                            if (repuesto.getPrecio() > pilaRepuestos.buscarPosicion(i-1).getPrecio()){
                                repuestosMasCaros.insertarPrincipio(repuesto);
                            }else{
                                repuestosMasCaros.insertarFinal(repuesto);
                            }
                        }else{
                            repuestosMasCaros.insertarFinal(repuesto);
                        }
                    }

                }

                int contadorRepuestos = 1;
                Repuesto rep = repuestosMasCaros.buscarPosicion(1);
                System.out.println("Repuesto: " + rep.getName());
                for (int i = 0; i < repuestosMasCaros.lengthLista(); i++) {
                    Repuesto imprimir = repuestosMasCaros.buscarPosicion(i);
                    if (imprimir != null){
                        writer.write("<tr>");
                        System.out.println(i);
                        writer.write("<th>" + imprimir.getName() + "</th>");
                        writer.write("<th>" + imprimir.getMarca() + "</th>");
                        writer.write("<th>" + imprimir.getModelo() + "</th>");
                        writer.write("<th>" + imprimir.getExistencias() + "</th>");
                        writer.write("<th> Q" + imprimir.getPrecio() + "</th>");
                        writer.write("</tr>");
                        if (contadorRepuestos == 10){
                            break;
                        }
                        contadorRepuestos++;
                    }

                }

                writer.write("</table>");

                break;

            case 3: // LOS 5 AUTOMOVILES MAS REPETIDOS
                ListaAutomoviles listaAutomoviles = Principal.listaAutomoviles;

                writer.write("<h1>5 AUTOMOVILES MAS REPETIDOS</h1>");
                writer.write("<hr />");

                writer.write("<table class='table'>");

                writer.write("<tr>");
                writer.write("<th>Marca</th>");
                writer.write("<th>Modelo</th>");
                writer.write("</tr>");

                int autoAnterior = 0;
                AutosMasRepetidos autosMasRepetidos = new AutosMasRepetidos();
                for (int i = 0; i < listaAutomoviles.lengthLista(); i++){
                    Automovil auto = listaAutomoviles.buscarPosicion(i);
                    int autoRepetido = 0;
                    for (int o = 0; o < listaAutomoviles.lengthLista(); o++){
                        Automovil newAuto = listaAutomoviles.buscarPosicion(o);
                        if (auto.getMarca().equals(newAuto.getMarca()) && auto.getModelo().equals(newAuto.getModelo())){
                            autoRepetido++;
                        }
                    }

                    if (autoRepetido > 0) {
                        if (autosMasRepetidos.lengthLista() > 0) {
                            if (autoAnterior > autoRepetido) {
                                autosMasRepetidos.insertarFinal(auto);
                            } else {
                                autosMasRepetidos.insertarPrincipio(auto);
                            }
                        } else {
                            autosMasRepetidos.insertarFinal(auto);
                        }
                    }
                    autoAnterior = autoRepetido;
                }

                int contadorAutos = 1;
                for (int i = 0; i < autosMasRepetidos.lengthLista(); i++) {
                    Automovil imprimir = autosMasRepetidos.buscarPosicion(i);
                    if (imprimir != null){
                        writer.write("<tr>");
                        System.out.println(i);
                        writer.write("<th>" + imprimir.getModelo() + "</th>");
                        writer.write("<th>" + imprimir.getMarca() + "</th>");
                        writer.write("</tr>");
                        if (contadorAutos == 5){
                            break;
                        }
                        contadorAutos++;
                    }

                }

                writer.write("</table>");
                break;
        }
        // --- END CONTENT

        writer.write("</body>");
        // --- END BODY

        // --- START FOOTER
        writer.write("</html>");
        // --- END FOOTER

        writer.close();

        File myFile = new File(destination);
        Desktop.getDesktop().open(myFile);
    }

}
