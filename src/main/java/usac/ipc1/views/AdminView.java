package usac.ipc1.views;

import usac.ipc1.Principal;

import javax.swing.*;
import java.awt.*;

public class AdminView extends JFrame {

    private JPanel topContainer, mainContainer;
    private JButton btnLogout, btnEmpleados, btnRepuestos, btnServicios, btnClientesAutos, btnProgresoAutos, btnReportes;

    public AdminView(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Administrador");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop();
        buildCenter();

        pack();
        setVisible(true);

    }

    public void buildTop(){
        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.RIGHT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);

        String logued = "";
        if (Principal.loguedInE != null)
            logued = Principal.loguedInE.getName();
        else if(Principal.loguedInC != null)
            logued = Principal.loguedInC.getNombre();

        btnLogout = new JButton("Cerrar sesión de '" + logued + "'");
        btnLogout.setFocusPainted(false);
        btnLogout.addActionListener(e -> {
            Principal.loguedInE = null;
            Principal.loguedInC = null;
            new LoginView();
            dispose();
        });

        topContainer.add(btnLogout);

        add(topContainer, BorderLayout.NORTH);
    }

    private void buildCenter(){

        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        // ---- TOP BUTTONS LINE
        btnEmpleados = new JButton("Empleados");
        btnEmpleados.setPreferredSize(new Dimension(200, 100));
        btnEmpleados.addActionListener(e -> {
            new AdminOperations(Table.TipoTabla.EMPLEADOS);
            dispose();
        });

        btnRepuestos = new JButton("Repuestos");
        btnRepuestos.setPreferredSize(new Dimension(200, 100));
        btnRepuestos.addActionListener(e -> {
            new AdminOperations(Table.TipoTabla.REPUESTOS);
            dispose();
        });

        btnServicios = new JButton("Servicios");
        btnServicios.setPreferredSize(new Dimension(200, 100));
        btnServicios.addActionListener(e -> {
            new AdminOperations(Table.TipoTabla.SERVICIOS);
            dispose();
        });

        JPanel topLine = new JPanel();
        FlowLayout flowTopLine = new FlowLayout();
        topLine.setLayout(flowTopLine);
        topLine.setMaximumSize(new Dimension(getWidth(), 150));
        topLine.setBackground(Color.WHITE);

        topLine.add(btnEmpleados);
        topLine.add(btnRepuestos);
        topLine.add(btnServicios);

        // BOTTOM BUTTONS LINE
        btnClientesAutos = new JButton("Clientes y autos");
        btnClientesAutos.setPreferredSize(new Dimension(200, 100));
        btnClientesAutos.addActionListener(e -> {
            new AdminOperations(Table.TipoTabla.CLIENTES);
            dispose();
        });

        btnProgresoAutos = new JButton("Progreso Autos");
        btnProgresoAutos.setPreferredSize(new Dimension(200, 100));
        btnProgresoAutos.addActionListener(e -> {
            new ProgresoAutos();
            dispose();
        });

        btnReportes = new JButton("Reportes");
        btnReportes.setPreferredSize(new Dimension(200, 100));
        btnReportes.addActionListener(e -> {
            new ReportesView();
            dispose();
        });

        JPanel bottomLine = new JPanel();
        FlowLayout flowBottomLine = new FlowLayout();
        bottomLine.setLayout(flowBottomLine);
        bottomLine.setMaximumSize(new Dimension(getWidth(), 150));
        bottomLine.setBackground(Color.WHITE);

        bottomLine.add(btnClientesAutos);
        bottomLine.add(btnProgresoAutos);
        bottomLine.add(btnReportes);


        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(topLine);
        mainContainer.add(bottomLine);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);

    }

}
