package usac.ipc1.views;

import usac.ipc1.reportes.HtmlGeneration;
import usac.ipc1.reportes.PdfGeneration;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class ReportesView extends JFrame {

    private JPanel mainContainer, botContainer;
    private JButton btnBack;

    public ReportesView(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Reportes");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildCenter();
        buildBot();

        pack();
        setVisible(true);
    }

    private void buildCenter(){
        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        JButton btnClientes = new JButton("Clientes");
        btnClientes.addActionListener(e -> new HtmlGeneration(1));

        JButton btnRepuestos = new JButton("Top 10 repuestos mas usados");
        btnRepuestos.addActionListener(e -> new PdfGeneration(1));

        JButton btnRepuestosCaros = new JButton("Top 10 repuestos mas caros");
        btnRepuestosCaros.addActionListener(e -> new HtmlGeneration(2));

        JButton btnServicios = new JButton("Top 10 servicios mas usados");
        btnServicios.addActionListener(e -> new PdfGeneration(2));

        JButton btnAutos = new JButton("Top 5 vehiculos mas repetidos");
        btnAutos.addActionListener(e -> new HtmlGeneration(3));

        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(btnClientes);
        mainContainer.add(btnRepuestos);
        mainContainer.add(btnRepuestosCaros);
        mainContainer.add(btnServicios);
        mainContainer.add(btnAutos);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);
    }

    private void buildBot() {
        botContainer = new JPanel();
        FlowLayout flowBot = new FlowLayout();
        flowBot.setAlignment(FlowLayout.LEFT);
        botContainer.setLayout(flowBot);
        botContainer.setBackground(Color.WHITE);
        botContainer.setBorder(new MatteBorder(1, 0, 0, 0, Color.black));

        btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            new AdminView();
            dispose();
        });

        botContainer.add(btnBack);

        add(botContainer, BorderLayout.SOUTH);

    }

}
