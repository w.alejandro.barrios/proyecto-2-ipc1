package usac.ipc1.views;

import usac.ipc1.dialogs.SelectServiceDialog;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class ClientOperations extends JFrame {

    private JPanel topContainer, mainContainer, botContainer, buttonsContainer;
    private JButton btnBack, btnAdd, btnDelete, btnUpdate;

    public ClientOperations(Table.TipoTabla tipoTabla) {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Operaciones");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildCenter(tipoTabla);
        buildBot();

        pack();
        setVisible(true);
    }

    private void buildCenter(Table.TipoTabla tipoTabla) {
        if (mainContainer != null)
            remove(mainContainer);

        Table tabla = new Table(tipoTabla);
        JScrollPane scrollPane = new JScrollPane(tabla);
        scrollPane.setMaximumSize(new Dimension(900, 184));

        mainContainer = new JPanel();
        mainContainer.setSize(500, 200);
        mainContainer.setBackground(Color.WHITE);

        GroupLayout groupLayout = new GroupLayout(mainContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        mainContainer.setLayout(groupLayout);

        buttonsContainer = new JPanel();
        FlowLayout flowButtons = new FlowLayout();
        flowButtons.setAlignment(FlowLayout.LEFT);
        buttonsContainer.setLayout(flowButtons);
        buttonsContainer.setBackground(Color.WHITE);

        btnAdd = new JButton("Ingresar al taller");
        btnAdd.setFocusPainted(false);
        btnAdd.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                new SelectServiceDialog(this, false, tabla.getSelectedRow()).setVisible(true);
            } else {
                JOptionPane.showMessageDialog(this,
                        "Debe seleccionar un elemento",
                        "Ingresar",
                        JOptionPane.WARNING_MESSAGE);
            }

        });

        if (tipoTabla == Table.TipoTabla.PROCESO)
            btnAdd.setVisible(false);

        buttonsContainer.add(btnAdd);

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(scrollPane)
                        .addComponent(buttonsContainer)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(scrollPane)
                        .addComponent(buttonsContainer)
        );

        add(mainContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    private void buildBot() {
        botContainer = new JPanel();
        FlowLayout flowBot = new FlowLayout();
        flowBot.setAlignment(FlowLayout.LEFT);
        botContainer.setLayout(flowBot);
        botContainer.setBackground(Color.WHITE);
        botContainer.setBorder(new MatteBorder(1, 0, 0, 0, Color.black));

        btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            new ClienteView();
            dispose();
        });

        botContainer.add(btnBack);

        add(botContainer, BorderLayout.SOUTH);

    }

}
