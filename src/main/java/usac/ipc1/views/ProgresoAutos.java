package usac.ipc1.views;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class ProgresoAutos extends JFrame {

    private JPanel botContainer, mainContainer;
    private JButton btnBack;

    public ProgresoAutos(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Progreso de automoviles");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildCenter();
        buildBot();

        pack();
        setVisible(true);
    }

    private void buildCenter(){
        if (mainContainer != null)
            remove(mainContainer);

        JLabel lblEspera = new JLabel("Cola de Espera");
        Table tablaEspera = new Table(Table.TipoTabla.COLAESPERA);
        JScrollPane scrollPane = new JScrollPane(tablaEspera);
        scrollPane.setMaximumSize(new Dimension(900, 150));

        JLabel lblServicio = new JLabel("En servicio");
        Table tablaServicio = new Table(Table.TipoTabla.PROCESO);
        JScrollPane scrollServicio = new JScrollPane(tablaServicio);
        scrollServicio.setMaximumSize(new Dimension(900, 150));

        JLabel lblListos = new JLabel("Listos");
        Table tablaListos = new Table(Table.TipoTabla.LISTOS);
        JScrollPane scrollListos = new JScrollPane(tablaListos);
        scrollListos.setMaximumSize(new Dimension(900, 150));

        mainContainer = new JPanel();
        mainContainer.setSize(500, 200);
        mainContainer.setBackground(Color.WHITE);

        GroupLayout groupLayout = new GroupLayout(mainContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        mainContainer.setLayout(groupLayout);

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(lblEspera)
                        .addComponent(scrollPane)
                        .addComponent(lblServicio)
                        .addComponent(scrollServicio)
                        .addComponent(lblListos)
                        .addComponent(scrollListos)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(lblEspera)
                        .addComponent(scrollPane)
                        .addComponent(lblServicio)
                        .addComponent(scrollServicio)
                        .addComponent(lblListos)
                        .addComponent(scrollListos)
        );

        add(mainContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    private void buildBot() {
        botContainer = new JPanel();
        FlowLayout flowBot = new FlowLayout();
        flowBot.setAlignment(FlowLayout.LEFT);
        botContainer.setLayout(flowBot);
        botContainer.setBackground(Color.WHITE);
        botContainer.setBorder(new MatteBorder(1, 0, 0, 0, Color.black));

        btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            new AdminView();
            dispose();
        });

        botContainer.add(btnBack);

        add(botContainer, BorderLayout.SOUTH);

    }

}
