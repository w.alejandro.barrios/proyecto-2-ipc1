package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Empleado;

import javax.swing.*;
import java.awt.*;

public class LoginView extends JFrame {

    private JPanel topContainer, mainContainer;
    private JButton btnLogin, btnRegister;
    private JLabel lblUsuario, lblPassword;
    private JTextField txtUsuario;
    private JPasswordField txtPassword;

    public LoginView(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(600, 600));
        setPreferredSize(new Dimension(600, 600));
        setTitle("Login");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop();
        buildCenter();

        pack();
        setVisible(true);

    }

    private void buildTop(){

        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.LEFT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);

        btnRegister = new JButton("Registrarse");
        btnRegister.setFocusPainted(false);
        btnRegister.addActionListener(e -> {
            new RegisterView();
            dispose();
        });

        topContainer.add(btnRegister);

        add(topContainer, BorderLayout.NORTH);

    }

    private void buildCenter(){

        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        // ---- Usuario

        lblUsuario = new JLabel("Usuario: ");
        txtUsuario = new JTextField();
        txtUsuario.setPreferredSize(new Dimension(175, 25));

        JPanel user = new JPanel();
        FlowLayout flowUser = new FlowLayout();
        user.setLayout(flowUser);
        user.setMaximumSize(new Dimension(getWidth(), 20));
        user.setBackground(Color.WHITE);

        user.add(lblUsuario);
        user.add(txtUsuario);

        // ---- Password
        lblPassword = new JLabel("Contraseña: ");
        txtPassword = new JPasswordField();
        txtPassword.setPreferredSize(new Dimension(175, 25));

        JPanel password = new JPanel();
        FlowLayout flowPassword = new FlowLayout();
        password.setLayout(flowPassword);
        password.setMaximumSize(new Dimension(getWidth(), 20));
        password.setBackground(Color.WHITE);

        password.add(lblPassword);
        password.add(txtPassword);

        // ---- Login

        btnLogin = new JButton("Iniciar Sesión");
        btnLogin.setFocusPainted(false);
        btnLogin.addActionListener(e -> {
            Empleado empleado = Principal.listaEmpleados.buscar(txtUsuario.getText());
            if (empleado != null){
                if (empleado.getPassword().equals(txtPassword.getText())){
                    switch (empleado.getRol()){
                        case 1:
                            Principal.loguedInE = empleado;
                            new MechanicView();
                            dispose();
                            break;

                        case 2:
                            Principal.loguedInE = empleado;
                            new AdminView();
                            dispose();
                            break;

                        case 3:

                            break;
                    }
                }else{
                    System.out.println("Password incorrecta " + empleado.getUsername());
                }
            }else{
                Cliente cliente = Principal.listaClientes.buscarUsername(txtUsuario.getText());
                if (cliente != null){
                    if (cliente.getPassword().equals(txtPassword.getText())){
                        System.out.println("BIENVENIDO CLIENTE " + cliente.getUsername());
                        Principal.loguedInC = cliente;
                        new ClienteView();
                        dispose();
                    }else{
                        System.out.println("Password incorrecta " + cliente.getUsername());
                    }
                }else{
                    System.out.println("DATOS INCORRECTOS");
                }
            }
        });

        JPanel login = new JPanel();
        FlowLayout flowLogin = new FlowLayout();
        flowLogin.setAlignment(FlowLayout.CENTER);
        login.setLayout(flowLogin);
        login.setMaximumSize(new Dimension(getWidth(), 20));
        login.setBackground(Color.WHITE);

        login.add(btnLogin);

        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(user);
        mainContainer.add(password);
        mainContainer.add(login);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);

    }

}
