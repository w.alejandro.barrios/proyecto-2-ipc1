package usac.ipc1.views;

import usac.ipc1.Init.ReadFile;
import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Empleado;
import usac.ipc1.beans.Servicio;
import usac.ipc1.dialogs.ClientesDialog;
import usac.ipc1.dialogs.EmpleadosDialog;
import usac.ipc1.dialogs.RepuestosDialog;
import usac.ipc1.dialogs.ServiciosDialog;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;

public class AdminOperations extends JFrame {

    private JPanel topContainer, mainContainer, botContainer, buttonsContainer;
    private JButton btnCharge, btnBack, btnAdd, btnDelete, btnUpdate;

    public AdminOperations(Table.TipoTabla tipoTabla) {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Operaciones");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop(tipoTabla);
        buildCenter(tipoTabla);
        buildBot();

        pack();
        setVisible(true);
    }

    private void buildTop(Table.TipoTabla tipoTabla) {
        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.RIGHT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);
        topContainer.setBorder(new MatteBorder(0, 0, 1, 0, Color.black));

        btnCharge = new JButton("Cargar");
        btnCharge.setFocusPainted(false);
        btnCharge.addActionListener(e -> readFile(tipoTabla));

        topContainer.add(btnCharge);

        add(topContainer, BorderLayout.NORTH);
    }

    private void buildCenter(Table.TipoTabla tipoTabla) {
        if (mainContainer != null)
            remove(mainContainer);

        Table tabla = new Table(tipoTabla);
        JScrollPane scrollPane = new JScrollPane(tabla);
        scrollPane.setMaximumSize(new Dimension(900, 184));

        mainContainer = new JPanel();
        mainContainer.setSize(500, 200);
        mainContainer.setBackground(Color.WHITE);

        GroupLayout groupLayout = new GroupLayout(mainContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        mainContainer.setLayout(groupLayout);

        buttonsContainer = new JPanel();
        FlowLayout flowButtons = new FlowLayout();
        flowButtons.setAlignment(FlowLayout.LEFT);
        buttonsContainer.setLayout(flowButtons);
        buttonsContainer.setBackground(Color.WHITE);

        btnAdd = new JButton("Agregar");
        btnAdd.setFocusPainted(false);
        btnAdd.addActionListener(e -> {
            switch (tipoTabla){
                case EMPLEADOS:
                    new EmpleadosDialog(this, false, tabla, false, 0).setVisible(true);
                    break;

                case REPUESTOS:
                    new RepuestosDialog(this, false, tabla, false, 0).setVisible(true);
                    break;

                case SERVICIOS:
                    new ServiciosDialog(this, false, tabla, false, 0).setVisible(true);
                    break;

                case CLIENTES:
                    new ClientesDialog(this, false, tabla, false, 0).setVisible(true);
                    break;
            }
        });

        btnUpdate = new JButton("Modificar");
        btnUpdate.setFocusPainted(false);
        btnUpdate.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                switch (tipoTabla) {
                    case EMPLEADOS:
                        new EmpleadosDialog(this, false, tabla, true, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case REPUESTOS:
                        new RepuestosDialog(this, false, tabla, true, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case SERVICIOS:
                        new ServiciosDialog(this, false, tabla, true, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case CLIENTES:
                        new ClientesDialog(this, false, tabla, true, tabla.getSelectedRow()).setVisible(true);
                        break;
                }
            }else{
                JOptionPane.showMessageDialog(this,
                        "Debe seleccionar la fila a actualizar",
                        "Editar fila",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        btnDelete = new JButton("Eliminar");
        btnDelete.setFocusPainted(false);
        btnDelete.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                if (JOptionPane.YES_OPTION ==
                        JOptionPane.showConfirmDialog(this,
                                "Desea eliminar la fila seleccionada?",
                                "Eliminar fila",
                                JOptionPane.YES_NO_OPTION)) {

                    switch (tipoTabla){
                        case EMPLEADOS:
                            Empleado empleado = Principal.listaEmpleados.buscarPosicion(tabla.getSelectedRow());
                            tabla.eliminar(empleado);
                            break;

                        case REPUESTOS:
                            if (tabla.getSelectedRow() > 0){
                                JOptionPane.showMessageDialog(this,
                                        "Sólo puedes eliminar el último ID",
                                        "Eliminar fila",
                                        JOptionPane.WARNING_MESSAGE);
                            }else{
                                Principal.pilaRepuestos.desapilar();
                            }
                            break;

                        case SERVICIOS:
                            Servicio servicio = Principal.listaServicios.buscarPosicion(tabla.getSelectedRow());
                            tabla.eliminar(servicio);
                            break;

                        case CLIENTESAUTOS:
                            Automovil automovil = Principal.listaAutomoviles.buscarPosicion(tabla.getSelectedRow());
                            tabla.eliminar(automovil);
                            break;

                        case CLIENTES:
                            Cliente cliente = Principal.listaClientes.buscarPosicion(tabla.getSelectedRow());
                            tabla.eliminar(cliente);
                            break;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this,
                        "Debe seleccionar la fila a eliminar",
                        "Eliminar fila",
                        JOptionPane.WARNING_MESSAGE);
            }

            buildCenter(tipoTabla);
        });

        buttonsContainer.add(btnAdd);
        buttonsContainer.add(btnUpdate);
        buttonsContainer.add(btnDelete);

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(scrollPane)
                        .addComponent(buttonsContainer)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(scrollPane)
                        .addComponent(buttonsContainer)
        );

        add(mainContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    private void buildBot() {
        botContainer = new JPanel();
        FlowLayout flowBot = new FlowLayout();
        flowBot.setAlignment(FlowLayout.LEFT);
        botContainer.setLayout(flowBot);
        botContainer.setBackground(Color.WHITE);
        botContainer.setBorder(new MatteBorder(1, 0, 0, 0, Color.black));

        btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            new AdminView();
            dispose();
        });

        botContainer.add(btnBack);

        add(botContainer, BorderLayout.SOUTH);

    }

    private void readFile(Table.TipoTabla tipoTabla) {
        JFileChooser jFileChooser = new JFileChooser("../Desktop/");
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilter filtro = null;
        switch (tipoTabla) {
            case EMPLEADOS:
                filtro = new FileNameExtensionFilter("Archivos TME (*.tme)", "tme");
                break;

            case REPUESTOS:
                filtro = new FileNameExtensionFilter("Archivos TMR (*.tmr)", "tmr");
                break;

            case SERVICIOS:
                filtro = new FileNameExtensionFilter("Archivos TMS (*.tms)", "tms");
                break;

            case CLIENTES: case CLIENTESAUTOS:
                filtro = new FileNameExtensionFilter("Archivos TMCA (*.tmca)", "tmca");
                break;
        }

        jFileChooser.setFileFilter(filtro);
        if (jFileChooser.showDialog(this, "Abrir archivo") == JFileChooser.APPROVE_OPTION) {
            new ReadFile(jFileChooser.getSelectedFile(), this, tipoTabla);
            buildCenter(tipoTabla);
        }
    }

}
