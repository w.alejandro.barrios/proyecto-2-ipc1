package usac.ipc1.views;

import usac.ipc1.Principal;

import javax.swing.*;
import java.awt.*;

public class MechanicView extends JFrame {

    private JPanel topContainer, mainContainer;
    private JButton btnLogout;

    public MechanicView(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Mecanico");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop();
        buildCenter();

        pack();
        setVisible(true);
    }

    public void buildTop(){
        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.RIGHT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);

        String logued = "";
        if (Principal.loguedInE != null)
            logued = Principal.loguedInE.getName();
        else if(Principal.loguedInC != null)
            logued = Principal.loguedInC.getNombre();

        btnLogout = new JButton("Cerrar sesión de '" + logued + "'");
        btnLogout.setFocusPainted(false);
        btnLogout.addActionListener(e -> {
            Principal.loguedInE = null;
            Principal.loguedInC = null;
            new LoginView();
            dispose();
        });

        topContainer.add(btnLogout);

        add(topContainer, BorderLayout.NORTH);
    }

    private void buildCenter(){

        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        JLabel lblCargo = new JLabel("Auto asignado");
        JTextField txtCargo = new JTextField();
        txtCargo.setPreferredSize(new Dimension(300, 30));
        txtCargo.setMaximumSize(new Dimension(300, 30));
        if (Principal.loguedInE.getOrden() != null)
            txtCargo.setText(Principal.loguedInE.getOrden().getAutomovil().getPlaca());
        txtCargo.setEnabled(false);

        JButton btnTerminar = new JButton("Terminar reparacion");
        btnTerminar.addActionListener(e -> {
            if (Principal.loguedInE.getOrden() != null){
                Principal.colaAutosListos.insertar(Principal.loguedInE.getOrden());
                Principal.contadorColaAutosListos++;
                Principal.loguedInE.getOrden().setEstado(3);
                Principal.loguedInE.setOrden(null);
                Principal.loguedInE.setOcupado(false);
                txtCargo.setText("");
                if (Principal.colaEspera.lengthCola() > 0){
                    // Asignar nuevo auto
                    Principal.loguedInE.setOcupado(true);
                    Principal.loguedInE.setOrden(Principal.colaEspera.procesar());
                    Principal.colaEspera.procesar().setEmpleado(Principal.loguedInE);
                    Principal.colaEspera.procesar().setEstado(2);
                    Principal.colaEspera.eliminar();
                    txtCargo.setText(Principal.loguedInE.getOrden().getAutomovil().getPlaca());
                }
            }
        });

        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(lblCargo);
        mainContainer.add(txtCargo);
        mainContainer.add(btnTerminar);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);

    }

}
