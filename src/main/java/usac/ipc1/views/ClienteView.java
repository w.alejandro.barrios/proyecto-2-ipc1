package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.dialogs.AutomovilesDialog;

import javax.swing.*;
import java.awt.*;

public class ClienteView extends JFrame {

    private JPanel topContainer, mainContainer;
    private JButton btnLogout, btnRegistrarAuto, btnAutos, btnProgreso, btnFacturas;

    public ClienteView(){
        super();

        setLayout(new BorderLayout());
        setBounds(new Rectangle(900, 600));
        setPreferredSize(new Dimension(900, 600));
        setTitle("Cliente");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop();
        buildCenter();

        pack();
        setVisible(true);

    }

    private void buildTop(){
        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.RIGHT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);

        String logued = "";
        if (Principal.loguedInE != null)
            logued = Principal.loguedInE.getName();
        else if(Principal.loguedInC != null)
            logued = Principal.loguedInC.getNombre();

        btnLogout = new JButton("Cerrar sesión de '" + logued + "'");
        btnLogout.setFocusPainted(false);
        btnLogout.addActionListener(e -> {
            Principal.loguedInE = null;
            Principal.loguedInC = null;
            new LoginView();
            dispose();
        });

        topContainer.add(btnLogout);

        add(topContainer, BorderLayout.NORTH);
    }

    private void buildCenter(){

        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        // ---- TOP BUTTONS LINE
        btnRegistrarAuto = new JButton("Registrar Automovil");
        btnRegistrarAuto.setPreferredSize(new Dimension(200, 100));
        btnRegistrarAuto.addActionListener(e -> new AutomovilesDialog(this, false, null, false, 0).setVisible(true));

        btnAutos = new JButton("Ver mis automoviles");
        btnAutos.setPreferredSize(new Dimension(200, 100));
        btnAutos.addActionListener(e -> {
            new ClientOperations(Table.TipoTabla.MISAUTOS);
            dispose();
        });

        JPanel topLine = new JPanel();
        FlowLayout flowTopLine = new FlowLayout();
        topLine.setLayout(flowTopLine);
        topLine.setMaximumSize(new Dimension(getWidth(), 150));
        topLine.setBackground(Color.WHITE);

        topLine.add(btnRegistrarAuto);
        topLine.add(btnAutos);

        // BOTTOM BUTTONS LINE
        btnProgreso = new JButton("Ver Progreso");
        btnProgreso.setPreferredSize(new Dimension(200, 100));
        btnProgreso.addActionListener(e -> {
            new ClientOperations(Table.TipoTabla.PROCESO);
            dispose();
        });

        btnFacturas = new JButton("Facturas");
        btnFacturas.setPreferredSize(new Dimension(200, 100));

        JPanel bottomLine = new JPanel();
        FlowLayout flowBottomLine = new FlowLayout();
        bottomLine.setLayout(flowBottomLine);
        bottomLine.setMaximumSize(new Dimension(getWidth(), 150));
        bottomLine.setBackground(Color.WHITE);

        bottomLine.add(btnProgreso);
        bottomLine.add(btnFacturas);


        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(topLine);
        mainContainer.add(bottomLine);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);

    }

}
