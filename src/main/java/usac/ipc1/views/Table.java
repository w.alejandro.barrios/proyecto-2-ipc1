package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;
import usac.ipc1.beans.Cliente;
import usac.ipc1.beans.Empleado;
import usac.ipc1.beans.Servicio;
import usac.ipc1.models.*;

import javax.swing.*;

public class Table extends JTable {

    public enum TipoTabla { EMPLEADOS, REPUESTOS, SERVICIOS, CLIENTESAUTOS, REPORTES, CLIENTES, MISAUTOS, COLAESPERA, LISTOS, PROCESO }
    private TipoTabla tipoTabla;

    public Table(){
        super();
    }

    public Table(TipoTabla tipoTabla){
        super();
        switch (tipoTabla){
            case EMPLEADOS:
                setModel(new TablaEmpleados(Principal.listaEmpleados));
                break;

            case REPUESTOS:
                setModel(new TablaRepuestos(Principal.pilaRepuestos));
                break;

            case SERVICIOS:
                setModel(new TablaServicios(Principal.listaServicios));
                break;

            case CLIENTESAUTOS:
                setModel(new TablaAutomoviles(Principal.listaAutomoviles));
                break;

            case REPORTES:

                break;

            case CLIENTES:
                setModel(new TablaClientes(Principal.listaClientes));
                break;

            case MISAUTOS:
                setModel(new TablaMisAutomoviles(Principal.listaAutomoviles));
                break;

            case COLAESPERA:
                setModel(new TablaColaEspera(Principal.colaEspera));
                break;

            case LISTOS:
                setModel(new TablaColaListos(Principal.colaAutosListos));
                break;

            case PROCESO:
                setModel(new TablaAutosAtencion(Principal.listaOrden, Principal.loguedInC));
                break;
        }
        this.tipoTabla = tipoTabla;
    }

    // EMPLEADO
    public void eliminar(Empleado empleado){
        Principal.listaEmpleados.eliminar(empleado);
        revalidate();
        repaint();
    }

    // SERVICIOS
    public void eliminar(Servicio servicio){
        Principal.listaServicios.eliminar(servicio);
        revalidate();
        repaint();
    }

    // AUTOMOVILES
    public void eliminar(Automovil automovil){
        Principal.listaAutomoviles.eliminar(automovil);
        revalidate();
        repaint();
    }

    // CLIENTE
    public void eliminar(Cliente cliente){
        Principal.listaClientes.eliminar(cliente);
        revalidate();
        repaint();
    }

}
