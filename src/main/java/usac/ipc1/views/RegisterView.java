package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;

import javax.swing.*;
import java.awt.*;

public class RegisterView extends JFrame {

    private JPanel topContainer, mainContainer;
    private JButton btnLogin, btnRegister;
    private JLabel lblUsuario, lblPassword, lblDpi, lblNombre;
    private JTextField txtUsuario, txtDpi, txtNombre;
    private JPasswordField txtPassword;

    public RegisterView(){
        super();

        setLayout(new BorderLayout());
        setBounds(new Rectangle(600, 600));
        setPreferredSize(new Dimension(600, 600));
        setTitle("Login");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTop();
        buildCenter();

        pack();
        setVisible(true);
    }

    private void buildTop(){

        topContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.LEFT);
        topContainer.setLayout(flowTop);
        topContainer.setBackground(Color.WHITE);

        btnLogin = new JButton("Login");
        btnLogin.setFocusPainted(false);
        btnLogin.addActionListener(e -> {
            new LoginView();
            dispose();
        });

        topContainer.add(btnLogin);

        add(topContainer, BorderLayout.NORTH);

    }

    private void buildCenter(){

        mainContainer = new JPanel();
        BoxLayout boxLayout = new BoxLayout(mainContainer, BoxLayout.PAGE_AXIS);
        mainContainer.setLayout(boxLayout);
        mainContainer.setBackground(Color.WHITE);

        // ---- Nombre

        lblNombre = new JLabel("Nombre: ");
        txtNombre = new JTextField();
        txtNombre.setPreferredSize(new Dimension(175, 25));

        JPanel nombre = new JPanel();
        FlowLayout flowNombre = new FlowLayout();
        nombre.setLayout(flowNombre);
        nombre.setMaximumSize(new Dimension(getWidth(), 20));
        nombre.setBackground(Color.WHITE);

        nombre.add(lblNombre);
        nombre.add(txtNombre);

        // ---- DPI

        lblDpi = new JLabel("DPI: ");
        txtDpi = new JTextField();
        txtDpi.setPreferredSize(new Dimension(175, 25));

        JPanel dpi = new JPanel();
        FlowLayout flowDpi = new FlowLayout();
        dpi.setLayout(flowDpi);
        dpi.setMaximumSize(new Dimension(getWidth(), 20));
        dpi.setBackground(Color.WHITE);

        dpi.add(lblDpi);
        dpi.add(txtDpi);

        // ---- Usuario

        lblUsuario = new JLabel("Usuario: ");
        txtUsuario = new JTextField();
        txtUsuario.setPreferredSize(new Dimension(175, 25));

        JPanel user = new JPanel();
        FlowLayout flowUser = new FlowLayout();
        user.setLayout(flowUser);
        user.setMaximumSize(new Dimension(getWidth(), 20));
        user.setBackground(Color.WHITE);

        user.add(lblUsuario);
        user.add(txtUsuario);

        // ---- Password
        lblPassword = new JLabel("Contraseña: ");
        txtPassword = new JPasswordField();
        txtPassword.setPreferredSize(new Dimension(175, 25));

        JPanel password = new JPanel();
        FlowLayout flowPassword = new FlowLayout();
        password.setLayout(flowPassword);
        password.setMaximumSize(new Dimension(getWidth(), 20));
        password.setBackground(Color.WHITE);

        password.add(lblPassword);
        password.add(txtPassword);

        // ---- Registrar

        btnRegister = new JButton("Registrarme");
        btnRegister.setFocusPainted(false);
        btnRegister.addActionListener(e -> {
            Cliente cliente = new Cliente(txtDpi.getText(), txtNombre.getText(), txtUsuario.getText(), txtPassword.getText(), 1, 0);
            Principal.listaClientes.insertarFinal(cliente);
            Principal.contadorClientes++;
            Principal.loguedInC = cliente;
            new ClienteView();
            dispose();
        });

        JPanel register = new JPanel();
        FlowLayout flowLogin = new FlowLayout();
        flowLogin.setAlignment(FlowLayout.CENTER);
        register.setLayout(flowLogin);
        register.setMaximumSize(new Dimension(getWidth(), 20));
        register.setBackground(Color.WHITE);

        register.add(btnRegister);

        mainContainer.add(Box.createVerticalGlue());
        mainContainer.add(nombre);
        mainContainer.add(dpi);
        mainContainer.add(user);
        mainContainer.add(password);
        mainContainer.add(register);
        mainContainer.add(Box.createVerticalGlue());

        add(mainContainer, BorderLayout.CENTER);

    }

}
