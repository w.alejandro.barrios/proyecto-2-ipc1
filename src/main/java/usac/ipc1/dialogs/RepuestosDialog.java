package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Repuesto;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class RepuestosDialog extends JDialog {

    Repuesto repuesto;

    public RepuestosDialog(JFrame padre, boolean modal, Table table, boolean isEdited, int position){
        super(padre, modal);
        setBounds(new Rectangle(500, 175));
        setLocationRelativeTo(null);
        setTitle("Repuestos");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblNombre = new JLabel("Nombre");
        JTextField txtNombre = new JTextField();

        JLabel lblMarca = new JLabel("Marca");
        JTextField txtMarca = new JTextField();

        JLabel lblModelo = new JLabel("Modelo");
        JTextField txtModelo = new JTextField();

        JLabel lblExistencias = new JLabel("Existencias");
        JTextField txtExistencias = new JTextField();

        JLabel lblPrecio = new JLabel("Precio");
        JTextField txtPrecio = new JTextField();

        if (isEdited){
            int buscado = Principal.pilaRepuestos.lengthPila() - position;
            repuesto = Principal.pilaRepuestos.buscarPosicion(buscado);
            txtNombre.setText(repuesto.getName());
            txtMarca.setText(repuesto.getMarca());
            txtModelo.setText(repuesto.getModelo());
            txtExistencias.setText("" + repuesto.getExistencias());
            txtPrecio.setText("" + repuesto.getPrecio());
        }

        JButton btnConfirmar = new JButton("Guardar");
        btnConfirmar.addActionListener(e -> {
            if (isEdited){
                repuesto.setName(txtNombre.getText());
                repuesto.setMarca(txtMarca.getText());
                repuesto.setModelo(txtModelo.getText());
                repuesto.setExistencias(Integer.parseInt(txtExistencias.getText()));
                repuesto.setPrecio(Double.parseDouble(txtPrecio.getText()));
            }else{
                Principal.pilaRepuestos.apilar(new Repuesto(
                        Principal.contadorRepuestos,
                        txtNombre.getText(),
                        txtMarca.getText(),
                        txtModelo.getText(),
                        Integer.parseInt(txtExistencias.getText()),
                        Double.parseDouble(txtPrecio.getText())
                ));
                Principal.contadorRepuestos++;
            }

            table.revalidate();
            table.repaint();
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblNombre);
        contenedor1.add(txtNombre);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblMarca);
        contenedor2.add(txtMarca);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblModelo);
        contenedor3.add(txtModelo);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblExistencias);
        contenedor4.add(txtExistencias);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblPrecio);
        contenedor5.add(txtPrecio);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(btnConfirmar);
        contenedor6.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);
        contenedor.add(contenedor3);
        contenedor.add(contenedor4);
        contenedor.add(contenedor5);
        contenedor.add(contenedor6);

        add(contenedor);

    }

}
