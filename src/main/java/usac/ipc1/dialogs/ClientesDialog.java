package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class ClientesDialog extends JDialog {

    Cliente cliente;

    public ClientesDialog(JFrame padre, boolean modal, Table table, boolean isEdited, int position){
        super(padre, modal);
        setBounds(new Rectangle(500, 175));
        setLocationRelativeTo(null);
        setTitle("Servicios");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblDpi = new JLabel("DPI");
        JTextField txtDpi = new JTextField();

        JLabel lblNombre = new JLabel("Nombre");
        JTextField txtNombre = new JTextField();

        JLabel lblUsername = new JLabel("Username");
        JTextField txtUsername = new JTextField();

        JLabel lblPassword = new JLabel("Password");
        JPasswordField txtPassword = new JPasswordField();

        JLabel lblTipo = new JLabel("Tipo");
        JComboBox cbTipo = new JComboBox();
        cbTipo.addItem("Normal");
        cbTipo.addItem("Oro");

        if (isEdited){
            cliente = Principal.listaClientes.buscarPosicion(position);
            txtDpi.setText(cliente.getDpi());
            txtNombre.setText(cliente.getNombre());
            txtUsername.setText(cliente.getUsername());
            txtPassword.setText(cliente.getPassword());
            if (cliente.getTipoCliente() == 1)
                cbTipo.setSelectedItem("Normal");
            else
                cbTipo.setSelectedItem("Oro");
        }

        JButton btnConfirmar = new JButton("Guardar");
        btnConfirmar.addActionListener(e -> {
            int rol = 0;
            if (cbTipo.getSelectedItem().toString().equals("Normal"))
                rol = 1;
            else
                rol = 2;

            if (isEdited){
                cliente.setDpi(txtDpi.getText());
                cliente.setNombre(txtNombre.getText());
                cliente.setUsername(txtUsername.getText());
                cliente.setPassword(txtPassword.getText());
                cliente.setTipoCliente(rol);
            }else{
                Principal.listaClientes.insertarFinal(new Cliente(
                        txtDpi.getText(),
                        txtNombre.getText(),
                        txtUsername.getText(),
                        txtPassword.getText(),
                        rol,
                        0
                ));
                Principal.contadorClientes++;
            }


            table.revalidate();
            table.repaint();
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblDpi);
        contenedor1.add(txtDpi);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblNombre);
        contenedor2.add(txtNombre);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblUsername);
        contenedor3.add(txtUsername);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblPassword);
        contenedor4.add(txtPassword);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblTipo);
        contenedor5.add(cbTipo);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(btnConfirmar);
        contenedor6.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);
        contenedor.add(contenedor3);
        contenedor.add(contenedor4);
        contenedor.add(contenedor5);
        contenedor.add(contenedor6);

        add(contenedor);
    }

}
