package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class AutomovilesDialog extends JDialog {

    Automovil automovil;

    public AutomovilesDialog(JFrame padre, boolean modal, Table table, boolean isEdited, int position){
        super(padre, modal);
        setBounds(new Rectangle(500, 175));
        setLocationRelativeTo(null);
        setTitle("Automovil");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblPlaca = new JLabel("Placa");
        JTextField txtPlaca = new JTextField();

        JLabel lblMarca = new JLabel("Marca");
        JTextField txtMarca = new JTextField();

        JLabel lblModelo = new JLabel("Modelo");
        JTextField txtModelo = new JTextField();

        JLabel lblImagen = new JLabel("Imagen");
        JTextField txtRutaImagen = new JTextField();

        if (isEdited){
            automovil = Principal.listaAutomoviles.buscarPosicion(position);
            txtPlaca.setText(automovil.getPlaca());
            txtMarca.setText(automovil.getMarca());
            txtModelo.setText(automovil.getModelo());
            txtRutaImagen.setText(automovil.getRutaImagen());
        }

        JButton btnConfirmar = new JButton("Registrar");
        btnConfirmar.addActionListener(e -> {
            if (isEdited){
                automovil.setPlaca(txtPlaca.getText());
                automovil.setMarca(txtMarca.getText());
                automovil.setModelo(txtModelo.getText());
                automovil.setRutaImagen(txtRutaImagen.getText());
            }else{
                Principal.listaAutomoviles.insertarFinal(new Automovil(
                        txtPlaca.getText(),
                        txtMarca.getText(),
                        txtModelo.getText(),
                        txtRutaImagen.getText(),
                        Principal.loguedInC
                ));
                Principal.contadorAutomoviles++;
            }
            if (table != null){
                table.revalidate();
                table.repaint();
            }

            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblPlaca);
        contenedor1.add(txtPlaca);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblMarca);
        contenedor2.add(txtMarca);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblModelo);
        contenedor3.add(txtModelo);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblImagen);
        contenedor4.add(txtRutaImagen);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(btnConfirmar);
        contenedor6.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);
        contenedor.add(contenedor3);
        contenedor.add(contenedor4);
        contenedor.add(contenedor6);

        add(contenedor);
    }

}
