package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Repuesto;
import usac.ipc1.beans.Servicio;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class ServiciosDialog extends JDialog {

    Servicio servicio;

    public ServiciosDialog(JFrame padre, boolean modal, Table table, boolean isEdited, int position){
        super(padre, modal);
        setBounds(new Rectangle(500, 175));
        setLocationRelativeTo(null);
        setTitle("Servicios");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblNombre = new JLabel("Nombre");
        JTextField txtNombre = new JTextField();

        JLabel lblMarca = new JLabel("Marca");
        JTextField txtMarca = new JTextField();

        JLabel lblModelo = new JLabel("Modelo");
        JTextField txtModelo = new JTextField();

        JLabel lblManoDeObra = new JLabel("Mano de obra");
        JTextField txtManoDeObra = new JTextField();

        JLabel lblRepuestos = new JLabel("Repuestos(Separados por ;)");
        JTextField txtRepuestos = new JTextField();

        if (isEdited){
            servicio = Principal.listaServicios.buscarPosicion(position);
            txtNombre.setText(servicio.getNombre());
            txtMarca.setText(servicio.getMarca());
            txtModelo.setText(servicio.getModelo());
            txtManoDeObra.setText("" + servicio.getManoDeObra());
            txtRepuestos.setText(servicio.getPilaRepuestos());
        }

        JButton btnConfirmar = new JButton("Guardar");
        btnConfirmar.addActionListener(e -> {

            Double precioRepuestos = 0.0;
            String[] repuesto = txtRepuestos.getText().split(";");
            for (int i = 0; i < repuesto.length; i++){
                Repuesto getRepuesto = Principal.pilaRepuestos.buscarRepuesto(Integer.parseInt(repuesto[i]));
                precioRepuestos += getRepuesto.getPrecio();
                getRepuesto.setExistencias(getRepuesto.getExistencias() - 1);
                getRepuesto.setUso(getRepuesto.getUso() + 1);
            }

            if (isEdited){
                servicio.setNombre(txtNombre.getText());
                servicio.setMarca(txtMarca.getText());
                servicio.setModelo(txtModelo.getText());
                servicio.setManoDeObra(Double.parseDouble(txtManoDeObra.getText()));
                servicio.setPilaRepuestos(txtRepuestos.getText());
                servicio.setPrecioTotal(precioRepuestos + Double.parseDouble(txtManoDeObra.getText()));
            }else{
                Principal.listaServicios.insertarFinal(new Servicio(
                        Principal.contadorServicios,
                        txtNombre.getText(),
                        txtMarca.getText(),
                        txtModelo.getText(),
                        Double.parseDouble(txtManoDeObra.getText()),
                        txtRepuestos.getText(),
                        precioRepuestos + Double.parseDouble(txtManoDeObra.getText())
                ));
                Principal.contadorServicios++;
            }

            table.revalidate();
            table.repaint();
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblNombre);
        contenedor1.add(txtNombre);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblMarca);
        contenedor2.add(txtMarca);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblModelo);
        contenedor3.add(txtModelo);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblManoDeObra);
        contenedor4.add(txtManoDeObra);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblRepuestos);
        contenedor5.add(txtRepuestos);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(btnConfirmar);
        contenedor6.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);
        contenedor.add(contenedor3);
        contenedor.add(contenedor4);
        contenedor.add(contenedor5);
        contenedor.add(contenedor6);

        add(contenedor);


    }

}
