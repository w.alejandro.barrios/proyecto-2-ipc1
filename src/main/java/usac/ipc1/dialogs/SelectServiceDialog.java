package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Automovil;
import usac.ipc1.beans.Empleado;
import usac.ipc1.beans.Orden;
import usac.ipc1.beans.Servicio;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class SelectServiceDialog extends JDialog {

    Automovil automovil;

    public SelectServiceDialog(JFrame padre, boolean modal, int posicion) {
        super(padre, modal);
        setBounds(new Rectangle(500, 110));
        setLocationRelativeTo(null);
        setTitle("Ingresar");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        automovil = Principal.listaAutomoviles.buscarPosicion(posicion);

        JLabel lblServicio = new JLabel("Selecciona el servicio que deseas");
        JComboBox cbServicios = new JComboBox();

        for (int i = 0; i < Principal.listaServicios.lengthLista(); i++) {
            cbServicios.addItem(Principal.listaServicios.buscarPosicion(i).getNombre());
        }

        JButton btnConfirmar = new JButton("Confirmar");
        btnConfirmar.addActionListener(e -> {
            Servicio servicio = Principal.listaServicios.buscarPosicion(cbServicios.getSelectedIndex());
            if ((automovil.getMarca().equals(servicio.getMarca())) &&
                    (automovil.getModelo().equals(servicio.getModelo()))) {

                Principal.loguedInC.setQuantityServicios(Principal.loguedInC.getQuantityServicios() + 1);
                if (Principal.loguedInC.getQuantityServicios() > 3) {
                    Principal.loguedInC.setTipoCliente(2); // Oro
                }

                Orden orden = new Orden(
                        Principal.contadorOrdenes,
                        automovil,
                        automovil.getCliente(),
                        servicio,
                        new Date(),
                        null,
                        1
                );

                Principal.listaOrden.insertarFinal(orden);
                Principal.contadorOrdenes++;

                Empleado empleado;

                // Verificar cola de espera
                if (Principal.colaEspera.lengthCola() > 0) {
                    // ENCOLARSE
                    agregarColaEspera(orden);
                } else {
                    boolean listo = false;
                    for (int i = 0; i < Principal.listaEmpleados.lengthLista(); i++) {
                        if (Principal.listaEmpleados.buscarPosicion(i).getRol() == 1){
                            if (!Principal.listaEmpleados.buscarPosicion(i).isOcupado()) {
                                empleado = Principal.listaEmpleados.buscarPosicion(i);
                                empleado.setOrden(orden);
                                orden.setEmpleado(empleado);
                                empleado.setOcupado(true);
                                orden.setEstado(2);
                                listo = true;
                                break;
                            }
                        }

                    }

                    if (!listo){
                        agregarColaEspera(orden);
                    }
                }

                dispose();

            } else {
                JOptionPane.showMessageDialog(this,
                        "Selecciona un servicio ideal para tu auto",
                        "Servicio",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500, 50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblServicio);
        contenedor1.add(cbServicios);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500, 50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(btnConfirmar);
        contenedor2.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);

        add(contenedor);
    }

    private void agregarColaEspera(Orden orden) {
        Principal.colaEspera.insertar(orden);
        System.out.println("ENCOLADO");
    }

}
