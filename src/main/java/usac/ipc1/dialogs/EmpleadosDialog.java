package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Empleado;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class EmpleadosDialog extends JDialog {

    Empleado empleado;

    public EmpleadosDialog(JFrame padre, boolean modal, Table tabla, boolean isEdited, int position){
        super(padre, modal);
        setBounds(new Rectangle(500, 175));
        setLocationRelativeTo(null);
        setTitle("Empleados");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblNombre = new JLabel("Nombre");
        JTextField txtNombre = new JTextField();

        JLabel lblUsername = new JLabel("Username");
        JTextField txtUsername = new JTextField();

        JLabel lblPassword = new JLabel("Password");
        JPasswordField txtPassword = new JPasswordField();

        JLabel lblRol = new JLabel("Rol");
        JComboBox cbRol = new JComboBox();
        cbRol.addItem("Mecanico");
        cbRol.addItem("Administrador");
        cbRol.addItem("Receptor/Pagador");

        if (isEdited){
            empleado = Principal.listaEmpleados.buscarPosicion(position);
            txtNombre.setText(empleado.getName());
            txtUsername.setText(empleado.getUsername());
            txtPassword.setText(empleado.getPassword());
            if (empleado.getRol() == 1)
                cbRol.setSelectedItem("Mecanico");
            else if(empleado.getRol() == 2)
                cbRol.setSelectedItem("Administrador");
            else
                cbRol.setSelectedItem("Receptor/Pagador");
        }

        JButton btnConfirmar = new JButton("Guardar");
        btnConfirmar.addActionListener(e -> {
            int rol = 0;
            switch (cbRol.getSelectedItem().toString()){
                case "Mecanico":
                    rol = 1;
                    break;

                case "Administrador":
                    rol = 2;
                    break;

                case "Receptor/Pagador":
                    rol = 3;
                    break;
            }

            if (isEdited){
                empleado.setName(txtNombre.getText());
                empleado.setUsername(txtUsername.getText());
                empleado.setPassword(txtPassword.getText());
                empleado.setRol(rol);
            }else{
                Principal.listaEmpleados.insertarFinal(new Empleado(
                        Principal.contadorEmpleados,
                        txtNombre.getText(),
                        rol,
                        txtUsername.getText(),
                        txtPassword.getText()
                ));
                Principal.contadorEmpleados++;
            }

            tabla.revalidate();
            tabla.repaint();
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblNombre);
        contenedor1.add(txtNombre);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblUsername);
        contenedor2.add(txtUsername);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblPassword);
        contenedor3.add(txtPassword);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblRol);
        contenedor4.add(cbRol);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(btnConfirmar);
        contenedor5.add(btnCancel);

        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.white);
        contenedor.setSize(500, 50);
        BoxLayout boxlayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxlayout);
        contenedor.add(contenedor1);
        contenedor.add(contenedor2);
        contenedor.add(contenedor3);
        contenedor.add(contenedor4);
        contenedor.add(contenedor5);

        add(contenedor);

    }

}
