package usac.ipc1.Nodos;

import usac.ipc1.beans.Automovil;

// Lista simple circular
public class ListaAutomoviles {

    private Nodo cabeza = null;
    private int longitud = 0;

    private class Nodo {
        public Automovil automovil;
        public Nodo siguiente = null;

        public Nodo(Automovil automovil) {
            this.automovil = automovil;
        }
    }

    public void insertarFinal(Automovil automovil) {
        Nodo nodo = new Nodo(automovil);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != cabeza) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
            nodo.siguiente = cabeza;
        } else {
            cabeza = nodo;
            cabeza.siguiente = cabeza;
        }
        longitud++;
    }

    public Automovil buscar(String placaAutomovil) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != cabeza) {
                if (puntero.automovil.getPlaca().equals(placaAutomovil)) {
                    return puntero.automovil;
                }
                puntero = puntero.siguiente;
            }
        }
        return null;
    }

    public Automovil buscarPosicion(int posicion) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != cabeza) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion){
                return null;
            }else{
                return puntero.automovil;
            }
        }
    }

    public void eliminar(Automovil automovil) {
        if (cabeza != null) {
            if (cabeza.automovil.getPlaca().equals(automovil.getPlaca())) {
                Nodo primero = cabeza;
                if (longitud == 1) {
                    cabeza.siguiente = null;
                    cabeza = null;
                } else {
                    cabeza = cabeza.siguiente;
                    primero.siguiente = null;
                }
            } else {
                Nodo puntero = cabeza.siguiente;
                int contador = 1;
                while (puntero != cabeza) {
                    if (puntero.automovil.getPlaca().equals(automovil.getPlaca())) {
                        int i = 0;
                        Nodo temporal = cabeza;
                        while (i < (contador - 1)) {
                            temporal = temporal.siguiente;
                            i++;
                        }
                        temporal.siguiente = puntero.siguiente;
                        puntero.siguiente = null;
                        break;
                    }
                    puntero = puntero.siguiente;
                    contador++;
                }
            }
            longitud--;
        }
    }

    public int lengthLista() {
        return longitud;
    }

}
