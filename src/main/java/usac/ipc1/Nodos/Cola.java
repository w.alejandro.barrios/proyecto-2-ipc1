package usac.ipc1.Nodos;

import usac.ipc1.beans.Orden;

public interface Cola {

    void insertar(Orden orden);

    void eliminar();

    Orden procesar();

}
