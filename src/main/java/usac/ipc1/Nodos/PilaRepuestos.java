package usac.ipc1.Nodos;

import usac.ipc1.beans.Repuesto;

public class PilaRepuestos implements Pila {

    private class Nodo{
        public Repuesto repuesto;
        public Nodo siguiente = null;

        public Nodo(Repuesto repuesto){
            this.repuesto = repuesto;
        }
    }

    private Nodo cima = null;
    private int longitud = 0;

    @Override
    public void apilar(Repuesto repuesto) {
        Nodo nodo = new Nodo(repuesto);
        nodo.siguiente = cima;
        cima = nodo;
        longitud++;
    }

    @Override
    public void desapilar() {
        if (cima != null){
            Nodo eliminar = cima;
            cima = cima.siguiente;
            eliminar.siguiente = null;
            longitud--;
        }
    }

    @Override
    public Repuesto obtener() {
        if (cima == null){
            return null;
        }else{
            return cima.repuesto;
        }
    }

    @Override
    public Repuesto recorrer(int posicion){
        if (cima == null) {
            return null;
        } else {
            Nodo puntero = cima;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.repuesto;
            }
        }
    }

    public Repuesto buscarPosicion(int posicion){
        if (cima == null) {
            return null;
        } else {
            Nodo puntero = cima;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.repuesto;
            }
        }
    }

    public Repuesto buscarRepuesto(int id){
        if (cima == null) {
            return null;
        } else {
            Nodo puntero = cima;
            while (puntero.siguiente != null) {
                if (puntero.repuesto.getIdRepuesto() == id) {
                    return puntero.repuesto;
                }
                puntero = puntero.siguiente;
            }
            if (puntero.repuesto.getIdRepuesto() == id){
                return puntero.repuesto;
            }
        }
        return null;
    }

    public int lengthPila(){
        return longitud;
    }
}
