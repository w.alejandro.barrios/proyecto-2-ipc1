package usac.ipc1.Nodos;

import usac.ipc1.beans.Repuesto;

public interface Pila {

    void apilar(Repuesto repuesto);

    void desapilar();

    Repuesto obtener();

    Repuesto recorrer(int posicion);

    Repuesto buscarRepuesto(int id);
}
