package usac.ipc1.Nodos;

import usac.ipc1.beans.Cliente;

// Lista doblemente circular
public class ListaClientes {

    private Nodo cabeza = null;
    private int longitud = 0;

    private class Nodo {
        public Cliente cliente;
        public Nodo siguiente = null;
        public Nodo anterior = null;

        public Nodo(Cliente cliente) {
            this.cliente = cliente;
        }
    }

    public void insertarFinal(Cliente cliente) {
        Nodo nodo = new Nodo(cliente);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != cabeza) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
            nodo.siguiente = cabeza;
            nodo.anterior = puntero;
            cabeza.anterior = nodo;
        } else {
            cabeza = nodo;
            cabeza.siguiente = cabeza;
            cabeza.anterior = cabeza;
        }
        longitud++;
    }

    public Cliente buscarDpi(String dpiCliente) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != cabeza) {
                if (puntero.cliente.getDpi().equals(dpiCliente)) {
                    return puntero.cliente;
                }
                puntero = puntero.siguiente;
            }
            if (puntero.cliente.getDpi().equals(dpiCliente)){
                return puntero.cliente;
            }
        }
        return null;
    }

    public Cliente buscarUsername(String username) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != cabeza) {
                if (puntero.cliente.getUsername().equals(username)) {
                    return puntero.cliente;
                }
                puntero = puntero.siguiente;
            }
            if (puntero.cliente.getUsername().equals(username)){
                return puntero.cliente;
            }
        }
        return null;
    }

    public Cliente buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != cabeza) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion){
                return null;
            }else{
                return puntero.cliente;
            }
        }
    }

    public void eliminar(Cliente cliente) {
        if (cabeza != null) {
            if (cabeza.cliente.getDpi() == cliente.getDpi()) {
                Nodo primero = cabeza;
                if (longitud == 1) {
                    cabeza.siguiente = null;
                    cabeza.anterior = null;
                    cabeza = null;
                } else {
                    cabeza = cabeza.siguiente;
                    cabeza.anterior = primero.anterior;
                    primero.siguiente = null;
                    primero.anterior = null;
                }
            } else {
                Nodo puntero = cabeza.siguiente;
                while (puntero != cabeza) {
                    if (puntero.cliente.getDpi() == cliente.getDpi()) {
                        Nodo temporal = puntero;
                        puntero = puntero.anterior;
                        puntero.siguiente = temporal.siguiente;
                        puntero = temporal.siguiente;
                        puntero.anterior = temporal.anterior;
                        temporal.anterior = null;
                        temporal.siguiente = null;
                        break;
                    }
                    puntero = puntero.siguiente;
                }
            }
            longitud--;
        }
    }

    public int lengthLista() {
        return longitud;
    }

}
