package usac.ipc1.Nodos;

import usac.ipc1.beans.Orden;

// Lista simplemente enlazada
public class ListaOrden {

    private Nodo cabeza = null;
    private int longitud;

    private class Nodo {
        public Orden orden;
        public Nodo siguiente = null;

        public Nodo(Orden orden) {
            this.orden = orden;
        }
    }

    public void insertarFinal(Orden orden) {
        Nodo nodo = new Nodo(orden);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Orden buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.orden;
            }
        }
    }

    public Orden buscar(int idOrden) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                if (puntero.orden.getIdOrden() == idOrden) {
                    return puntero.orden;
                }
                puntero = puntero.siguiente;

            }
        }
        return null;
    }

    public int lengthLista() {
        return longitud;
    }

    public void eliminar(Orden orden) {
        if (cabeza != null) {
            if (cabeza.orden.getIdOrden() == orden.getIdOrden()) {
                Nodo primero = cabeza;
                cabeza = cabeza.siguiente;
                primero.siguiente = null;
            } else {
                Nodo puntero = cabeza;
                int contador = 0;
                while (puntero.siguiente != null) {
                    if (puntero.orden.getIdOrden() == orden.getIdOrden()) {
                        int i = 0;
                        Nodo temporal = cabeza;
                        while (i < (contador - 1)) {
                            temporal = temporal.siguiente;
                            i++;
                        }
                        temporal.siguiente = puntero.siguiente;
                        puntero.siguiente = null;

                    } else {
                        puntero = puntero.siguiente;
                    }
                    contador++;

                }
            }
        }
    }

}
