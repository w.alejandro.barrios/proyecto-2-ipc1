package usac.ipc1.Nodos;


import usac.ipc1.beans.Empleado;

// Lista doblemente enlazada
public class ListaEmpleados {

    private Nodo cabeza = null;
    private int longitud = 0;

    private class Nodo {
        public Empleado empleado;
        public Nodo siguiente = null;
        public Nodo anterior = null;

        public Nodo(Empleado empleado) {
            this.empleado = empleado;
        }
    }

    public void insertarFinal(Empleado empleado) {
        Nodo nodo = new Nodo(empleado);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
            nodo.anterior = puntero;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Empleado buscar(int id) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                if (puntero.empleado.getIdEmpleado() == id) {
                    return puntero.empleado;
                }
                puntero = puntero.siguiente;
            }
            if (puntero.empleado.getIdEmpleado() == id) {
                return puntero.empleado;
            }
        }
        return null;
    }

    public Empleado buscar(String username) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                if (puntero.empleado.getUsername().equals(username)) {
                    return puntero.empleado;
                }
                puntero = puntero.siguiente;
            }
            if (puntero.empleado.getUsername().equals(username)) {
                return puntero.empleado;
            }
        }
        return null;
    }

    public Empleado buscarPosicion(int posicion) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.empleado;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

    public void eliminar(Empleado empleado) {
        if (cabeza != null) {
            if (cabeza.empleado.getIdEmpleado() == empleado.getIdEmpleado()) {
                Nodo primero = cabeza;
                cabeza = cabeza.siguiente;
                cabeza.anterior = null;
                primero.siguiente = null;
            } else {
                Nodo puntero = cabeza;
                while (puntero != null) {
                    if (puntero.empleado.getIdEmpleado() == empleado.getIdEmpleado()) {
                        Nodo temporal = puntero;
                        puntero = puntero.anterior;
                        puntero.siguiente = temporal.siguiente;
                        puntero = temporal.siguiente;
                        if (puntero != null)
                            puntero.anterior = temporal.anterior;
                        temporal.anterior = null;
                        temporal.siguiente = null;

                    }
                    if (puntero != null) {
                        if (puntero.siguiente != null)
                            puntero = puntero.siguiente;
                    }

                }

            }
            longitud--;
        }
    }

}
