package usac.ipc1.Nodos;

import usac.ipc1.beans.Servicio;

// Lista simplemente enlazada
public class ListaServicios {

    private Nodo cabeza = null;
    private int longitud;

    private class Nodo {
        public Servicio servicio;
        public Nodo siguiente = null;

        public Nodo(Servicio servicio) {
            this.servicio = servicio;
        }
    }

    public void insertarFinal(Servicio servicio) {
        Nodo nodo = new Nodo(servicio);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Servicio buscar(int idServicio) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                if (puntero.servicio.getIdServicio() == idServicio) {
                    return puntero.servicio;
                }
                puntero = puntero.siguiente;

            }
        }
        return null;
    }

    public Servicio buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.servicio;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

    public void eliminar(Servicio servicio) {
        if (cabeza != null) {
            if (cabeza.servicio.getIdServicio() == servicio.getIdServicio()) {
                Nodo primero = cabeza;
                cabeza = cabeza.siguiente;
                primero.siguiente = null;
            } else {
                Nodo puntero = cabeza;
                int contador = 0;
                while (puntero != null) {
                    if (puntero.servicio.getIdServicio() == servicio.getIdServicio()) {
                        int i = 0;
                        Nodo temporal = cabeza;
                        while (i < (contador - 1)) {
                            System.out.println(contador);
                            temporal = temporal.siguiente;
                            i++;
                        }
                        temporal.siguiente = puntero.siguiente;
                        puntero.siguiente = null;

                    }
                    puntero = puntero.siguiente;
                    contador++;

                }
            }
            longitud--;
        }
    }

}
