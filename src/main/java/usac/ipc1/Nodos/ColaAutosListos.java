package usac.ipc1.Nodos;

import usac.ipc1.beans.Orden;

public class ColaAutosListos implements Cola {

    private class Nodo{
        public Orden orden;
        public Nodo siguiente = null;

        public Nodo(Orden orden){
            this.orden = orden;
        }
    }

    private Nodo cabeza, ultimo;
    private int longitud = 0;

    @Override
    public void insertar(Orden orden) {
        Nodo nodo = new Nodo(orden);
        if (cabeza == null){
            cabeza = nodo;
        }else{
            ultimo.siguiente = nodo;
        }
        ultimo = nodo;
        longitud++;
    }

    @Override
    public void eliminar() {
        if (cabeza != null){
            Nodo eliminar = cabeza;
            cabeza = cabeza.siguiente;
            eliminar.siguiente = null;
            if (cabeza == null){
                ultimo = null;
            }
            longitud--;
        }
    }

    public Orden recorrer(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.orden;
            }
        }
    }

    @Override
    public Orden procesar() {
        if (cabeza == null){
            return null;
        }else{
            return cabeza.orden;
        }
    }

    public int lengthCola(){
        return longitud;
    }

}
