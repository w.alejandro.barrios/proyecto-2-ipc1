package usac.ipc1.ordenamientos;

import usac.ipc1.beans.Servicio;

public class ServiciosMasUsados {

    private Nodo cabeza = null;
    private int longitud;

    private class Nodo {
        public Servicio servicio;
        public Nodo siguiente = null;

        public Nodo(Servicio servicio) {
            this.servicio = servicio;
        }
    }

    public void insertarPrincipio(Servicio servicio){
        Nodo nodo = new Nodo(servicio);
        nodo.siguiente = cabeza;
        cabeza = nodo;
        longitud++;
    }

    public void insertarFinal(Servicio servicio) {
        Nodo nodo = new Nodo(servicio);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Servicio buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.servicio;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

}
