package usac.ipc1.ordenamientos;

import usac.ipc1.beans.Automovil;

public class AutosMasRepetidos {

    private Nodo cabeza = null;
    private int longitud;

    private class Nodo {
        public Automovil automovil;
        public Nodo siguiente = null;

        public Nodo(Automovil servicio) {
            this.automovil = automovil;
        }
    }

    public void insertarPrincipio(Automovil automovil){
        Nodo nodo = new Nodo(automovil);
        nodo.siguiente = cabeza;
        cabeza = nodo;
        longitud++;
    }

    public void insertarFinal(Automovil automovil) {
        Nodo nodo = new Nodo(automovil);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Automovil buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.automovil;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

}
