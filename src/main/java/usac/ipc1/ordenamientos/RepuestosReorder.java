package usac.ipc1.ordenamientos;

import usac.ipc1.beans.Repuesto;

public class RepuestosReorder {

    private Nodo cabeza = null;
    private int longitud;

    private class Nodo {
        public Repuesto repuesto;
        public Nodo siguiente = null;

        public Nodo(Repuesto repuesto) {
            this.repuesto = repuesto;
        }
    }

    public void insertarPrincipio(Repuesto repuesto){
        Nodo nodo = new Nodo(repuesto);
        nodo.siguiente = cabeza;
        cabeza = nodo;
        longitud++;
    }

    public void insertarFinal(Repuesto repuesto) {
        Nodo nodo = new Nodo(repuesto);
        if (cabeza != null) {
            Nodo puntero = cabeza;
            while (puntero.siguiente != null) {
                puntero = puntero.siguiente;
            }
            puntero.siguiente = nodo;
        } else {
            nodo.siguiente = cabeza;
            cabeza = nodo;
        }

        longitud++;
    }

    public Repuesto buscarPosicion(int posicion){
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.repuesto;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

}
